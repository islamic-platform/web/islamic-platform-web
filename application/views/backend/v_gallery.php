<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    // $this->load->view('backend/inc/v_header.php');
    // $this->load->view('backend/inc/v_menu.php');
    $this->load->view('backend/inc/v_sidebar.php');
?>

    <div class="page-wrapper">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Gallery Masjid</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pengaturan</a></li>
                        <li class="breadcrumb-item active">Gallery Masjid</li>
                    </ol>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tambahkan Gambar Tentang Masjid</h4>
                                <h6 class="card-subtitle">Ukuran yang disarankan adalah <code>100x100</code> sebanyak 10 buah.</h6>
                                <form action="<?=base_url()?>admin/settings/exec_gallery" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-md-12">Gambar</label>
                                        <div class="col-md-12">
                                            <input name="image" type="file" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Deskripsi</label>
                                        <div class="col-md-12">
                                            <input type="text" name="image-desc" class="form-control form-control-line" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success">Tambahkan Gambar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Daftar Gambar</h4>
                                <div class="table-responsive m-t-40">
                                    <style>
                                        td, th {
                                            text-align: left !important;
                                        }
                                        .table .square {
                                            position: relative;
                                            width: 400px;     
                                            height: 200px;
                                            overflow: hidden;
                                            margin:5px 15px 5px 5px;
                                        }
                                        .table img {
                                            position: absolute;
                                            max-width: 100%;
                                            width: 100%;
                                            height: auto;
                                            top: 50%;     
                                            left: 50%;
                                            transform: translate( -50%, -50%);
                                            margin:0px auto;
                                        }
                                        .table img.landscape {
                                            height: 100%;
                                            width: auto;
                                        }
                                    </style>
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Gambar</th>
                                                <th>Deskripsi</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Gambar</th>
                                                <th>Deskripsi</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php foreach ($gallery as $g) { ?>
                                                <tr>
                                                    <td>
                                                        <div class="square">
                                                            <img src="<?=base_url()?>assets/uploads/<?=$g->file_name?>" alt="<?=$g->file_name?>">
                                                        </div>                                                        
                                                    </td>
                                                    <td><?=$g->caption?></td>
                                                    <td>
                                                        <a href="<?=base_url()?>admin/settings/exec_delete_gallery/<?=$g->id_gallery?>">
                                                            <button type="button" class="btn btn-danger m-b-10 m-l-5">Hapus</button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php
    $this->load->view('backend/inc/v_footer.php');
?>
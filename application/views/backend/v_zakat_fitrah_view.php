<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('backend/inc/v_sidebar.php');
?>

    <div class="page-wrapper">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">List Data Zakat Fitrah</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Zakat Fitrah</a></li>
                        <li class="breadcrumb-item active">List Data Zakat Fitrah</li>
                    </ol>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            
							<a href="<?php echo base_url(); ?>admin/zakatfitrah/add/" class="btn btn-primary btn-rounded m-b-10 m-l-5">+ Tambah Data Zakat Fitrah</a>
                             
							<a target="_blank" href="<?php echo base_url();?>cetak/zakatfitrah_lap/" class="btn btn-primary btn-rounded m-b-10 m-l-5" >Cetak Laporan Penerimaan</a>
							<a target="_blank" href="<?php echo base_url();?>cetak/zakatfitrah_rekap/" class="btn btn-primary btn-rounded m-b-10 m-l-5" >Cetak Rekapitulasi</a>
							
							 <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal</th>
                                                <th>Nama</th>
                                                <th>Jiwa</th>
                                                <th>Jenis</th>
                                                <th>Jumlah</th>
                                                <th>Info</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal</th>
                                                <th>Nama</th>
                                                <th>Jiwa</th>
                                                <th>Jenis</th>
                                                <th>Jumlah</th>
                                                <th>Info</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                        <?php 
                                            $no = 1;
                                            foreach($zakat as $z){ 
                                        ?>

                                            <tr>
                                                <td><?php echo $no++ ?></td>
                                                <td><?php echo $z->date ?></td>
                                                <td><?php echo $z->name ?></td>
                                                <td><?php echo $z->jiwa ?></td>
                                                <td><?php echo $z->category ?></td>
                                                <td><?php echo $z->total ?></td>
                                                <td><?php echo $z->info ?></td>
                                                <td>
                                                    
                                                    <a href="<?php echo base_url(). 'admin/zakatfitrah/delete/' .$z->id_zakat_fitrah; ?>"><button type="button" class="btn btn-danger m-b-10 m-l-5">Hapus</button></a>
                                                    
                                                
                                                    <a target="_blank" href="<?php echo base_url();?>cetak/zakatfitrah/<?php echo $z->id_zakat_fitrah; ?>" class="btn btn-primary m-b-10 m-l-5" >Cetak Tanda Terima</a>
                                                    
                                                </td>
                                            </tr>
                                        
                                        <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php
    $this->load->view('backend/inc/v_footer.php');
?>
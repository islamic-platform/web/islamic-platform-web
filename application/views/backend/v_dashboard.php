<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('backend/inc/v_sidebar.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Laporan</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Manajemen</a></li>
                <li class="breadcrumb-item active">Halaman Utama Admin</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-arrow-circle-right f-s-40 color-primary"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2><?=$sambutan?></h2>
                            <p class="m-b-0" id="date">
                                <!-- 12 Agustus 2018 / 12 Syawal 1439 -->
                            </p>
                        </div>
                        <script>
                            const months = ["Januari", "Februari", "Maret","April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
                            var d = new Date();
                            var date = d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear();
                            document.getElementById('date').innerHTML = date;
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-usd f-s-40 color-primary"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2>Rp. <?=$infaq_mingguan?></h2>
                            <p class="m-b-0">Total Infaq Mingguan</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-usd f-s-40 color-success"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2>Rp. <?=$infaq_jumat?></h2>
                            <p class="m-b-0">Total Infaq Jum'at</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-archive f-s-40 color-warning"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2><?=$zakat_fitrah?> KG</h2>
                            <p class="m-b-0">Total Zakat Fitrah</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-archive f-s-40 color-danger"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2>Rp. <?=$zakat_mal?></h2>
                            <p class="m-b-0">Total Zakat Mal</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-user f-s-40 color-success"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2><?=$jamaah?></h2>
                            <p class="m-b-0">Total Jama'ah Masjid</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-user f-s-40 color-warning"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2><?=$takmir?></h2>
                            <p class="m-b-0">Total Pengurus Masjid</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-file f-s-40 color-success"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2><?=$berita?></h2>
                            <p class="m-b-0">Total Berita</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-file f-s-40 color-warning"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2><?=$kegiatan?></h2>
                            <p class="m-b-0">Total Kegiatan</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    $this->load->view('backend/inc/v_footer.php');
?>
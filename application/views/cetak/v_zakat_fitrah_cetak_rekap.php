<!--<script>


window.print();

</script>-->

<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=RekapZakatFitrah.xls");
?>
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>
<h3 align="center">Rekapitulasi Penerimaan Zakat Fitrah </h3>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td><strong>No</strong></td>
    <td><strong>Zakat</strong></td>
    <td><strong>Jumlah Jiwa</strong></td>
    <td><strong>Jenis</strong></td>
    <td><strong>Total</strong></td>
  </tr>

  <tr>
    <td>1</td>
    <td>Fitrah</td>
    <td><?php echo isset($field->jiwa_beras)?$field->jiwa_beras:''; ?></td>
    <td>Beras</td>
    <td><?php echo isset($field->total_beras)?$field->total_beras:''; ?></td>
  </tr>
  <tr>
    <td>2</td>
    <td>Fitrah</td>
    <td><?php echo isset($field2->jiwa_uang)?$field2->jiwa_uang:''; ?></td>
    <td>Uang</td>
    <td><?php echo isset($field2->total_uang)?$field2->total_uang:''; ?></td>
  </tr>
  
</table>
<br />

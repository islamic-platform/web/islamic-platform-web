<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mustahik extends AdminInterface {
	function __construct(){
		parent::__construct();		
		$this->load->model('backend/m_mustahik');
	}

	public function index(){
		$data['mustahik'] = $this->m_mustahik->view_mustahik_data()->result();
		$this->load->view('backend/v_mustahik_view', $data);
	}

	public function add(){
		$this->load->view('backend/v_mustahik_add');
	}

	function exec_add(){
		$date = $this->input->post('date-mustahik');
		$name = $this->input->post('name-mustahik');
		$category = $this->input->post('category-mustahik');
		$total = $this->input->post('number-mustahik');
		$info = $this->input->post('text-mustahik');

		$data = array(
			'date' => $date,
			'name' => $name,
			'category' => $category,
			'total' => $total,
			'info' => $info
		);
		$this->m_mustahik->insert_mustahik_data($data);
		redirect('admin/mustahik/');
	}

	function delete($id){
		$where = array('id_mustahik' => $id);
		$this->m_mustahik->delete_mustahik_data($where);
		redirect('admin/mustahik/');
	}
}